package com.bon.persistence;

import java.sql.Connection;

public final class JDBCTransactionManager {
	private static final ThreadLocal<JDBCTransaction> threadLocal =
			new ThreadLocal<JDBCTransaction>();
	
	public static synchronized JDBCTransaction begin() {
		JDBCTransaction t = threadLocal.get();
		if (t != null) {
			throw new RuntimeException("Já existe transação nessa thread");
		}
		t = new JDBCTransaction();
		threadLocal.set(t);
		return t;
	}
	
	public static synchronized void end() {
		if (threadLocal.get() == null) {
			throw new RuntimeException("Já não existe mais transação nessa thread");
		}
		threadLocal.set(null);
	}
	
	public static Connection getConnection() {
		JDBCTransaction t = threadLocal.get();
		if (t == null) {
			return JDBCTransaction.getConnection();
		}
		return t.getJconn();
	}
}
