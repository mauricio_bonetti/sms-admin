package com.bon.persistence;

import java.sql.Connection;


public abstract class JDBCDAO {
	protected Connection getConnection() {
		return JDBCTransactionManager.getConnection();
	}
}
