package com.bon.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bon.vo.CustomerVO;

public final class CustomerDAO extends JDBCDAO {
	private static CustomerDAO inst;
	
	private CustomerDAO() {}
	
	public static CustomerDAO getInstance() {
		if (inst == null) {
			inst = new CustomerDAO();
		}
		return inst;
	}
	
	public String findName(long objectId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT CUSTOMER_NAME FROM SMS_CUSTOMER WHERE CUSTOMER_ID = ?");
			stmt.setLong(1, objectId);
			rs = stmt.executeQuery();
			String name = null;
			if (rs.next()) {
				name = rs.getString("CUSTOMER_NAME");
			}
			stmt.close();
			rs.close();
			conn.close();
			return name;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<CustomerVO> findAllActive(long userObjectId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_MSISDN FROM SMS_CUSTOMER WHERE CUSTOMER_USER_ID = ? AND CUSTOMER_ACTIVE = TRUE");
			stmt.setLong(1, userObjectId);
			rs = stmt.executeQuery();
			List<CustomerVO> list = new ArrayList<CustomerVO>();
			while (rs.next()) {
				CustomerVO vo = new CustomerVO();
				vo.setObjectId(rs.getLong("CUSTOMER_ID"));
				vo.setUserObjectId(userObjectId);
				vo.setName(rs.getString("CUSTOMER_NAME"));
				vo.setMsisdn(rs.getString("CUSTOMER_MSISDN"));
				list.add(vo);
			}
			stmt.close();
			rs.close();
			conn.close();
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void update(CustomerVO vo) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("UPDATE SMS_CUSTOMER SET CUSTOMER_NAME = ?, CUSTOMER_MSISDN = ? WHERE CUSTOMER_ID = ?");
			stmt.setString(1, vo.getName());
			stmt.setString(2, vo.getMsisdn());
			stmt.setLong(3, vo.getObjectId());
			int r = stmt.executeUpdate();
			if (r != 1) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}

	public void insert(CustomerVO vo) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("INSERT INTO SMS_CUSTOMER(CUSTOMER_USER_ID, CUSTOMER_NAME, CUSTOMER_MSISDN, CUSTOMER_ACTIVE) VALUES(?, ?, ?, TRUE)");
			stmt.setLong(1, vo.getUserObjectId());
			stmt.setString(2, vo.getName());
			stmt.setString(3, vo.getMsisdn());
			int r = stmt.executeUpdate();
			if (r != 1) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}				
	}

	public CustomerVO find(long objectId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT CUSTOMER_USER_ID, CUSTOMER_NAME, CUSTOMER_MSISDN FROM SMS_CUSTOMER WHERE CUSTOMER_ID = ?");
			stmt.setLong(1, objectId);
			rs = stmt.executeQuery();
			CustomerVO vo = null;
			if (rs.next()) {
				vo = new CustomerVO();
				vo.setObjectId(objectId);
				vo.setUserObjectId(rs.getLong("CUSTOMER_USER_ID"));
				vo.setName(rs.getString("CUSTOMER_NAME"));
				vo.setMsisdn(rs.getString("CUSTOMER_MSISDN"));
			}
			stmt.close();
			rs.close();
			conn.close();
			return vo;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void deactivate(long objectId) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("UPDATE SMS_CUSTOMER SET CUSTOMER_ACTIVE = FALSE WHERE CUSTOMER_ID = ?");
			stmt.setLong(1, objectId);
			if (stmt.executeUpdate() == 0) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one or more lines. Changed zero");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}
}
