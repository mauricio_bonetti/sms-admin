package com.bon.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bon.vo.SMSVO;

public final class SMSDAO extends JDBCDAO {
	private static SMSDAO inst;
	
	private SMSDAO() {}
	
	public static SMSDAO getInstance() {
		if (inst == null) {
			inst = new SMSDAO();
		}
		return inst;
	}
	
	public List<SMSVO> findAll(long userObjectId, long firstTimestamp,
			long lastTimestamp) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT * FROM SMS_SMS WHERE SMS_USER_ID = ? AND SMS_SCHEDULED_TS >= ? AND SMS_SCHEDULED_TS <= ? ORDER BY SMS_SCHEDULED_TS");
			stmt.setLong(1, userObjectId);
			stmt.setTimestamp(2, new Timestamp(firstTimestamp));
			stmt.setTimestamp(3, new Timestamp(lastTimestamp));
			List<SMSVO> list = new ArrayList<SMSVO>();
			rs = stmt.executeQuery();
			while (rs.next()) {
				SMSVO vo = new SMSVO();
				vo.setObjectId(rs.getLong("SMS_ID"));
				vo.setUserObjectId(rs.getLong("SMS_USER_ID"));
				vo.setCustomerObjectId(rs.getLong("SMS_CUSTOMER_ID"));
				vo.setCreationTimestamp(rs.getTimestamp("SMS_CREATION_TS").getTime());
				vo.setScheduledTimestamp(rs.getTimestamp("SMS_SCHEDULED_TS").getTime());
				vo.setMessage(rs.getString("SMS_MESSAGE"));
				vo.setSent(rs.getBoolean("SMS_SENT"));
				list.add(vo);
			}
			stmt.close();
			rs.close();
			conn.close();
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void insert(SMSVO vo) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("INSERT INTO SMS_SMS(SMS_USER_ID, SMS_CREATION_TS, SMS_SCHEDULED_TS, SMS_CUSTOMER_ID, SMS_MESSAGE, SMS_SENT) VALUES(?, ?, ?, ?, ?, ?)");
			stmt.setLong(1, vo.getUserObjectId());
			stmt.setTimestamp(2, new Timestamp(vo.getCreationTimestamp()));
			stmt.setTimestamp(3, new Timestamp(vo.getScheduledTimestamp()));
			stmt.setLong(4, vo.getCustomerObjectId());
			stmt.setString(5, vo.getMessage());
			stmt.setBoolean(6, vo.isSent());
			int r = stmt.executeUpdate();
			if (r != 1) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}

	public void update(SMSVO vo) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("UPDATE SMS_SMS SET SMS_CREATION_TS = ?, SMS_SCHEDULED_TS = ?, SMS_CUSTOMER_ID = ?, SMS_MESSAGE = ?, SMS_SENT = ? WHERE SMS_ID = ?");
			stmt.setTimestamp(1, new Timestamp(vo.getCreationTimestamp()));
			stmt.setTimestamp(2, new Timestamp(vo.getScheduledTimestamp()));
			stmt.setLong(3, vo.getCustomerObjectId());
			stmt.setString(4, vo.getMessage());
			stmt.setBoolean(5, vo.isSent());
			stmt.setLong(6, vo.getObjectId());
			int r = stmt.executeUpdate();
			if (r != 1) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}	
	}

	public void delete(long objectId) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("DELETE FROM SMS_SMS WHERE SMS_ID = ?");
			stmt.setLong(1, objectId);
			int r = stmt.executeUpdate();
			if (r != 1) {
				conn.rollback();
				stmt.close();
				conn.close();
				throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
			}
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}				
	}

	public SMSVO find(long objectId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT * FROM SMS_SMS WHERE SMS_ID = ?");
			stmt.setLong(1, objectId);
			SMSVO vo = null;
			rs = stmt.executeQuery();
			if (rs.next()) {
				vo = new SMSVO();
				vo.setObjectId(rs.getLong("SMS_ID"));
				vo.setUserObjectId(rs.getLong("SMS_USER_ID"));
				vo.setCustomerObjectId(rs.getLong("SMS_CUSTOMER_ID"));
				vo.setCreationTimestamp(rs.getTimestamp("SMS_CREATION_TS").getTime());
				vo.setScheduledTimestamp(rs.getTimestamp("SMS_SCHEDULED_TS").getTime());
				vo.setMessage(rs.getString("SMS_MESSAGE"));
				vo.setSent(rs.getBoolean("SMS_SENT"));
			}
			stmt.close();
			conn.close();
			return vo;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void deleteByCustomerId(long customerObjectId) {
		PreparedStatement stmt = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("DELETE FROM SMS_SMS WHERE SMS_CUSTOMER_ID = ? AND SMS_SCHEDULED_TS >= ?");
			stmt.setLong(1, customerObjectId);
			stmt.setTimestamp(2, new Timestamp(new Date().getTime()));
			stmt.executeUpdate();
			stmt.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}
}
