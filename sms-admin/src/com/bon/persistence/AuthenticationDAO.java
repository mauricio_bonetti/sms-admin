package com.bon.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bon.vo.UserVO;

public final class AuthenticationDAO extends JDBCDAO {
	private static AuthenticationDAO inst;
	
	private AuthenticationDAO() {}
	
	public static AuthenticationDAO getInstance() {
		if (inst == null) {
			inst = new AuthenticationDAO();
		}
		return inst;
	}
	
	public UserVO login(String email, String password) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = getConnection();
		try {
			stmt = conn.prepareStatement("SELECT * FROM SEC_USER WHERE USER_EMAIL = ? AND USER_PASSWORD = ? AND USER_ACTIVE = TRUE");
			stmt.setString(1, email);
			stmt.setString(2, password);
			UserVO user = null;
			rs = stmt.executeQuery();
			if (rs.next()) {
				user = new UserVO();
				user.setObjectId(rs.getLong("USER_ID"));
				user.setName(rs.getString("USER_NAME"));
				user.setEmail(email);
				user.setPassword(password);
			}
			stmt.close();
			rs.close();
			conn.close();
			return user;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
