package com.bon.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateUtil {
	public static String format(long timestamp) {
		DateFormat df = new SimpleDateFormat("dd MMM yyyy", new Locale("pt", "BR"));
		return df.format(new Date(timestamp));
	}
	
	private static long set(long timestamp, int hour, int minute, int second, int millisecond) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, second);
		c.set(Calendar.MILLISECOND, millisecond);
		return c.getTimeInMillis();
	}
	
	public static long getDaysFirstTimestamp(long timestamp) {
		return set(timestamp, 0, 0, 0, 0);
	}
	
	public static long getDaysLastTimestamp(long timestamp) {
		return set(timestamp, 23, 59, 59, 999);
	}

	public static String getHoursAndMinutes(long timestamp) {
		DateFormat df = new SimpleDateFormat("HH:mm", new Locale("pt", "BR"));
		return df.format(new Date(timestamp));
	}

	public static boolean isTimeValid(String time) {
		int[] int_time = split(time);
		return ((int_time[0] < 24) && (int_time[1] < 60));
	}

	private static int[] split(String time) {
		String[] _time = time.split(":");
		int[] int_time = {Integer.parseInt(_time[0]), Integer.parseInt(_time[1])};
		return int_time;
	}
	
	public static long setFormattedTime(long timestamp, String time) {
		int[] int_time = split(time);
		long x = set(timestamp, int_time[0], int_time[1], 0, 0);
		return x;
	}
}
