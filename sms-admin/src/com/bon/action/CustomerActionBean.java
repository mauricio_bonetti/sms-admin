package com.bon.action;

import java.util.List;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

import com.bon.ctrl.CustomerCtrl;
import com.bon.vo.CustomerVO;

public class CustomerActionBean extends BaseActionBean {
	private List<CustomerVO> customers;
	
	@DontValidate
	@DefaultHandler
    public Resolution main() {
		CustomerCtrl c = CustomerCtrl.getInstance();
		customers = c.findAllActive(getUserFromSession().getObjectId());
		return new ForwardResolution(JSP_DIR + "customer.jsp");
    }
	
	public List<CustomerVO> getCustomers() {
		return customers;
	}
	public void setCustomers(List<CustomerVO> customers) {
		this.customers = customers;
	}
}
