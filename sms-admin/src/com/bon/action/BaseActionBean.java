package com.bon.action;

import java.util.Date;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

import com.bon.context.ApplicationContext;
import com.bon.vo.UserVO;

public abstract class BaseActionBean implements ActionBean {
    private ApplicationContext context;
    protected static final String JSP_DIR = "/page/jsp/";
    
    public ActionBeanContext getContext() {
        return context;
    }

    public void setContext(ActionBeanContext context) {
        this.context = (ApplicationContext) context;
    }
    
    protected boolean isUserLogged() {
        return (context.getUserFromSession() != null);
    }
    
    protected void setUserInSession(UserVO user) {
        context.setUserInSession(user);
    }
    
    protected long getCurrentTimestamp() {
    	return new Date().getTime();
    }
    
    protected UserVO getUserFromSession() {
    	return context.getUserFromSession();
    }
    
    @DontValidate
    public Resolution _logout() {
    	context.logout();
    	return new RedirectResolution(LoginActionBean.class);
    }
}
