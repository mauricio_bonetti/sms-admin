package com.bon.action;

import com.bon.ctrl.CustomerCtrl;
import com.bon.vo.CustomerVO;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

public class CustomerAddActionBean extends BaseActionBean {
	@ValidateNestedProperties({
		@Validate(field="name", label="Nome", required=true, maxvalue=84),
		@Validate(field="msisdn", label="Celular", required=true, minvalue=10, maxvalue=11, mask="[0-9]{10,11}")
	})
	private CustomerVO customer;
	
	@DontValidate
	@DefaultHandler
    public Resolution main() {
		return new ForwardResolution(JSP_DIR + "customerAdd.jsp");
    }
	
	@Validate
	public Resolution save() {
		customer.setUserObjectId(getUserFromSession().getObjectId());
		CustomerCtrl.getInstance().save(customer);
		return backToCustomer();
	}
	
	@DontValidate
	public Resolution cancel() {
		return backToCustomer();
	}

	private Resolution backToCustomer() {
		return new ForwardResolution(CustomerActionBean.class);
	}

	public CustomerVO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerVO customer) {
		this.customer = customer;
	}
}
