package com.bon.action;

import java.util.List;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

import com.bon.ctrl.SMSCtrl;
import com.bon.util.DateUtil;
import com.bon.vo.SMSVO;

public class SMSActionBean extends BaseActionBean {
	private long timestamp;
	private List<SMSVO> smss;
	
	@DefaultHandler
    public Resolution main() {
		if (timestamp <= 0) {
			timestamp = getCurrentTimestamp();
		}
		SMSCtrl c = SMSCtrl.getInstance();
		smss = c.findAll(getUserFromSession().getObjectId(), timestamp);
		return new ForwardResolution(JSP_DIR + "sms.jsp");
    }
	
	public String getFormattedDate() {
		return DateUtil.format(timestamp);
	}
	
	public boolean isDayPast() {
		return DateUtil.getDaysFirstTimestamp(getCurrentTimestamp()) > timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public List<SMSVO> getSmss() {
		return smss;
	}

	public void setSmss(List<SMSVO> smss) {
		this.smss = smss;
	}
}
