package com.bon.action;

import java.util.Date;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.Validate;

import com.bon.annotation.DoesNotRequireLogin;
import com.bon.ctrl.AuthenticationCtrl;
import com.bon.vo.UserVO;

@DoesNotRequireLogin
public class LoginActionBean extends BaseActionBean {
	@Validate (required=true)
	private String email;
	@Validate (required=true, label="Senha")
	private String password;
	
	@DefaultHandler
    @DontValidate
    public Resolution main() {
		if (isUserLogged()) {
        	return new ForwardResolution(SMSActionBean.class);
        }
		return new ForwardResolution(JSP_DIR + "login.jsp");
    }
	
	@Validate
	public Resolution login() {
		UserVO vo = AuthenticationCtrl.getInstance().login(email, password);
		if (vo != null) {
			setUserInSession(vo);
			return new ForwardResolution(SMSActionBean.class).addParameter("timestamp", new Date().getTime());	
		} else {
			getContext().getValidationErrors().addGlobalError(new SimpleError("Email/Senha incorreto"));
			return getContext().getSourcePageResolution();
		}
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}
