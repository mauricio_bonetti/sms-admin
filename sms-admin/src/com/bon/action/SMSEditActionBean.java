package com.bon.action;

import java.util.Date;
import java.util.List;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;

import com.bon.ctrl.CustomerCtrl;
import com.bon.ctrl.SMSCtrl;
import com.bon.util.DateUtil;
import com.bon.vo.CustomerVO;
import com.bon.vo.SMSVO;

public class SMSEditActionBean extends BaseActionBean {
	@Validate(required=true)
	private long timestamp;
	@Validate(required=true, label="Horário")
	private String scheduledTime;
	@Validate(required=true, maxlength=160, label="Mensagem")
	private String message;
	@Validate(required=true, label="Cliente")
	private long customerObjectId;
	private long completeScheduledTime;
	@Validate(required=true, on="main")
	private long objectId;
	private SMSVO sms;
	
	@DontValidate
	@DefaultHandler
    public Resolution main() {
		sms = SMSCtrl.getInstance().find(objectId);
		return new ForwardResolution(JSP_DIR + "smsEdit.jsp");
    }
	
	@ValidationMethod
	public void validate(ValidationErrors errors) {
		if (DateUtil.isTimeValid(scheduledTime)) {
			completeScheduledTime = DateUtil.setFormattedTime(timestamp, scheduledTime);
			if (completeScheduledTime <= timestamp) {
				errors.addGlobalError(new SimpleError("Horário inválido"));
			}
		} else {
			errors.addGlobalError(new SimpleError("Horário inválido"));
		}
	}
	
	@Validate
	public Resolution save() {
		SMSVO vo = new SMSVO();
		vo.setObjectId(objectId);
		vo.setCreationTimestamp(new Date().getTime());
		vo.setUserObjectId(getUserFromSession().getObjectId());
		vo.setCustomerObjectId(customerObjectId);
		vo.setMessage(message);
		vo.setScheduledTimestamp(completeScheduledTime);
		vo.setSent(false);
		SMSCtrl.getInstance().save(vo);
		return backToSMS();
    }
	
	@DontValidate
	public Resolution cancel() {
		return backToSMS();
	}
	
	private Resolution backToSMS() {
		return new ForwardResolution(SMSActionBean.class).addParameter("timestamp", timestamp);
	}

	public List<CustomerVO> getActiveCustomers() {
		CustomerCtrl c = CustomerCtrl.getInstance();
		return c.findAllActive(getUserFromSession().getObjectId());
	}

	public long getTimestamp() {
		return timestamp;
	}
	
	public String getFormattedDate() {
		return DateUtil.format(timestamp);
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getCustomerObjectId() {
		return customerObjectId;
	}

	public void setCustomerObjectId(long customerObjectId) {
		this.customerObjectId = customerObjectId;
	}
	
	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}

	public SMSVO getSms() {
		return sms;
	}

	public void setSms(SMSVO sms) {
		this.sms = sms;
	}
}
