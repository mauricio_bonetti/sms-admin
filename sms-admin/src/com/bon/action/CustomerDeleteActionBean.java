package com.bon.action;

import com.bon.ctrl.CustomerCtrl;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;

public class CustomerDeleteActionBean extends BaseActionBean {
	@Validate(required=true)
	private long objectId;
	
	@DefaultHandler
    public Resolution delete() {
		CustomerCtrl.getInstance().delete(objectId);
		return new ForwardResolution(CustomerActionBean.class);
    }

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}
}
