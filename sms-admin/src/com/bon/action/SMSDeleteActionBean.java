package com.bon.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;

import com.bon.ctrl.SMSCtrl;
import com.bon.util.DateUtil;

public class SMSDeleteActionBean extends BaseActionBean {
	@Validate(required=true)
	private long scheduledTimestamp;
	@Validate(required=true)
	private long timestamp;
	@Validate(required=true)
	private long objectId;
	
	@Validate
	@DefaultHandler
    public Resolution delete() {
		SMSCtrl.getInstance().delete(objectId);
		return new ForwardResolution(SMSActionBean.class).addParameter("timestamp", timestamp);
    }
	
	@ValidationMethod
	public void validate(ValidationErrors errors) {
		if (scheduledTimestamp <= getCurrentTimestamp()) {
			throw new RuntimeException("Cannot remove an already sent sms");
		}
	}
	
	public String getFormattedDate() {
		return DateUtil.format(timestamp);
	}

	public long getScheduledTimestamp() {
		return scheduledTimestamp;
	}

	public void setScheduledTimestamp(long scheduledTimestamp) {
		this.scheduledTimestamp = scheduledTimestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}
}
