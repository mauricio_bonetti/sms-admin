package com.bon.ctrl;

import java.util.List;

import com.bon.persistence.JDBCTransaction;
import com.bon.persistence.JDBCTransactionManager;
import com.bon.service.SMSService;
import com.bon.vo.SMSVO;

public final class SMSCtrl {
	private static SMSCtrl inst;
	
	private SMSCtrl() {}
	
	public static SMSCtrl getInstance() {
		if (inst == null) {
			inst = new SMSCtrl();
		}
		return inst;
	}
	
	public List<SMSVO> findAll(long userObjectId, long timestamp) {
		return SMSService.getInstance().findAll(userObjectId, timestamp);
	}
	
	public SMSVO find(long objectId) {
		return SMSService.getInstance().find(objectId);
	}

	public void save(SMSVO vo) {
		SMSService.getInstance().save(vo);
	}

	public void delete(long objectId) {
		SMSService.getInstance().delete(objectId);
	}
}
