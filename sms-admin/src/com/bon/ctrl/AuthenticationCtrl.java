package com.bon.ctrl;

import com.bon.service.AuthenticationService;
import com.bon.vo.UserVO;

public final class AuthenticationCtrl {
	private static AuthenticationCtrl inst;
	
	private AuthenticationCtrl() {}
	
	public static AuthenticationCtrl getInstance() {
		if (inst == null) {
			inst = new AuthenticationCtrl();
		}
		return inst;
	}
	
	public UserVO login(String email, String password) {
		return AuthenticationService.getInstance().login(email, password);
	}
}
