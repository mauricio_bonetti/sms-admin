package com.bon.ctrl;

import java.util.List;

import com.bon.persistence.JDBCTransaction;
import com.bon.persistence.JDBCTransactionManager;
import com.bon.service.CustomerService;
import com.bon.vo.CustomerVO;

public final class CustomerCtrl {
	private static CustomerCtrl inst;
	
	private CustomerCtrl() {}
	
	public static CustomerCtrl getInstance() {
		if (inst == null) {
			inst = new CustomerCtrl();
		}
		return inst;
	}
	
	public List<CustomerVO> findAllActive(long userObjectId) {
		return CustomerService.getInstance().findAllActive(userObjectId);
	}

	public void save(CustomerVO vo) {
		CustomerService.getInstance().save(vo);		
	}

	public CustomerVO find(long objectId) {
		return CustomerService.getInstance().find(objectId);
	}

	public void delete(long objectId) {
		JDBCTransaction t = JDBCTransactionManager.begin();
		try {
			CustomerService.getInstance().delete(objectId);
			t.commit();			
		} catch(Exception e) {
			t.rollback();
			throw new RuntimeException(e);
		} finally {
			t.close();
			JDBCTransactionManager.end();			
		}
	}
}
