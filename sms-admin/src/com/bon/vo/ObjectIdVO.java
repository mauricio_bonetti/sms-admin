package com.bon.vo;

public class ObjectIdVO {
	private long objectId;

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}
}
