package com.bon.vo;

public class CustomerVO extends ObjectIdVO {
	private long userObjectId;
	private String name;
	private String msisdn;
	
	public long getUserObjectId() {
		return userObjectId;
	}
	
	public void setUserObjectId(long userObjectId) {
		this.userObjectId = userObjectId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
}
