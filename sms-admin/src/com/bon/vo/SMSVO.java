package com.bon.vo;

import java.util.Date;

import com.bon.persistence.CustomerDAO;
import com.bon.util.DateUtil;

public class SMSVO extends ObjectIdVO {
	private long userObjectId;
	private long customerObjectId;
	private long creationTimestamp;
	private long scheduledTimestamp;
	private String message;
	private boolean sent;
	
	public long getUserObjectId() {
		return userObjectId;
	}
	
	public void setUserObjectId(long userObjectId) {
		this.userObjectId = userObjectId;
	}
	
	public long getCustomerObjectId() {
		return customerObjectId;
	}
	
	public String getCustomerName() {
		return CustomerDAO.getInstance().findName(customerObjectId);
	}
	
	public void setCustomerObjectId(long customerObjectId) {
		this.customerObjectId = customerObjectId;
	}
	
	public long getCreationTimestamp() {
		return creationTimestamp;
	}
	
	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	public long getScheduledTimestamp() {
		return scheduledTimestamp;
	}
	
	public String getScheduledTimestampFormatted() {
		return DateUtil.getHoursAndMinutes(scheduledTimestamp);
	}
	
	public void setScheduledTimestamp(long scheduledTimestamp) {
		this.scheduledTimestamp = scheduledTimestamp;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSent() {
		return sent;
	}
	
	public boolean isScheduledTimePast() {
		return  new Date().getTime() >= scheduledTimestamp;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}
}
