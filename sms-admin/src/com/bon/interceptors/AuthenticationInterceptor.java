package com.bon.interceptors;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.ExecutionContext;
import net.sourceforge.stripes.controller.Interceptor;
import net.sourceforge.stripes.controller.Intercepts;
import net.sourceforge.stripes.controller.LifecycleStage;

import com.bon.action.LoginActionBean;
import com.bon.annotation.DoesNotRequireLogin;
import com.bon.context.ApplicationContext;

@Intercepts(LifecycleStage.HandlerResolution)
public class AuthenticationInterceptor implements Interceptor {

	public Resolution intercept(ExecutionContext ctx) throws Exception {
		Class bean = ctx.getActionBean().getClass();
		if (bean.isAnnotationPresent(DoesNotRequireLogin.class)) {
			return ctx.proceed();
		} else {
			ApplicationContext actx = (ApplicationContext) ctx.getActionBeanContext();
			if (actx.getUserFromSession() != null) {
				return ctx.proceed();
			} else {
				return new RedirectResolution(LoginActionBean.class);
			}
		}
	}
}
