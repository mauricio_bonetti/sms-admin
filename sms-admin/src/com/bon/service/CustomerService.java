package com.bon.service;

import java.util.List;

import com.bon.persistence.CustomerDAO;
import com.bon.persistence.SMSDAO;
import com.bon.vo.CustomerVO;

public final class CustomerService {
	private static CustomerService inst;
	
	private CustomerService() {}
	
	public static CustomerService getInstance() {
		if (inst == null) {
			inst = new CustomerService();
		}
		return inst;		
	}
	
	public List<CustomerVO> findAllActive(long userObjectId) {
		return CustomerDAO.getInstance().findAllActive(userObjectId);
	}

	public void save(CustomerVO vo) {
		CustomerDAO dao = CustomerDAO.getInstance();
		if (vo.getObjectId() > 0) {
			dao.update(vo);
		} else {
			dao.insert(vo);
		}
	}

	public CustomerVO find(long objectId) {
		return CustomerDAO.getInstance().find(objectId);
	}

	public void delete(long objectId) {
		CustomerDAO.getInstance().deactivate(objectId);
		SMSDAO.getInstance().deleteByCustomerId(objectId);
	}
}
