package com.bon.service;

import com.bon.persistence.AuthenticationDAO;
import com.bon.vo.UserVO;

public final class AuthenticationService {
	private static AuthenticationService inst;
	
	private AuthenticationService() {}
	
	public static AuthenticationService getInstance() {
		if (inst == null) {
			inst = new AuthenticationService();
		}
		return inst;		
	}
	
	public UserVO login(String email, String password) {
		return AuthenticationDAO.getInstance().login(email, password);
	}
}
