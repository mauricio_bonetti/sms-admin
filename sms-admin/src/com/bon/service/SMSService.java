package com.bon.service;

import java.util.List;

import com.bon.persistence.SMSDAO;
import com.bon.util.DateUtil;
import com.bon.vo.SMSVO;

public final class SMSService {
	private static SMSService inst;
	
	private SMSService() {}
	
	public static SMSService getInstance() {
		if (inst == null) {
			inst = new SMSService();
		}
		return inst;		
	}
	
	public List<SMSVO> findAll(long userObjectId, long timestamp) {
		return SMSDAO.getInstance().findAll(userObjectId, DateUtil.getDaysFirstTimestamp(timestamp), DateUtil.getDaysLastTimestamp(timestamp));
	}

	public void save(SMSVO vo) {
		SMSDAO dao = SMSDAO.getInstance();
		if (vo.getObjectId() > 0) {
			dao.update(vo);
		} else {
			dao.insert(vo);
		}
	}

	public void delete(long objectId) {
		SMSDAO.getInstance().delete(objectId);		
	}

	public SMSVO find(long objectId) {
		return SMSDAO.getInstance().find(objectId);
	}
}
