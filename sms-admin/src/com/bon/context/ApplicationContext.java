package com.bon.context;

import javax.servlet.http.HttpSession;
import com.bon.vo.UserVO;
import net.sourceforge.stripes.action.ActionBeanContext;

public class ApplicationContext extends ActionBeanContext {
    public final static String USER = "user";
    
    public void setUserInSession(UserVO user) {
        getRequest().getSession().setAttribute(USER, user);
    }
    
    public UserVO getUserFromSession() {
        return (UserVO) getRequest().getSession().getAttribute(USER);
    }
    
    public void logout() {
        HttpSession s = getRequest().getSession();
        s.invalidate();
    }
}
