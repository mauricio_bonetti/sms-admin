<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@taglib  prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Torpedo-Avisa</title>
<link rel="stylesheet" type="text/css" href="page/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="page/css/commons.css"/>
<link rel="stylesheet" type="text/css" href="page/css/css3-gmail-style.css"/>
<link rel="stylesheet" type="text/css" href="page/css/content.css"/>
<link rel="stylesheet" type="text/css" href="page/css/header.css"/>
<link rel="stylesheet" type="text/css" href="page/css/menu.css"/>
</head>
<body>
	<!-- Header -->
	<%@ include file="tiles/header.jsp" %>
	<!-- Menu -->
	<%@ include file="tiles/menu.jsp" %>
	<!-- Content -->
	<div class="content common" style="width:420px">
		<span>SMS</span>
		<div style="margin-top:20px;">
			<span>Adicionar SMS para o dia ${actionBean.formattedDate}</span>
		</div>
		<div style="margin-top:10px; margin-right:15px; margin-bottom:15px;">
			<s:form beanclass="com.bon.action.SMSAddActionBean" method="POST">
				<s:hidden name="timestamp" value="${actionBean.timestamp}"></s:hidden>
				<div class="margin">Horário: <s:text id="scheduledTime" style="width:40px" name="scheduledTime" class="textField"></s:text></div>			
				<div class="margin">Cliente: 
					<s:select name="customerObjectId" class="textField">					
						<s:option value="">Selecione</s:option>
						<s:options-collection collection="${actionBean.activeCustomers}" value="objectId" label="name" sort="name"/>
					</s:select>
				</div>
				<c:set var="messageLength" value="${fn:length(actionBean.message)}"></c:set>
				<div class="margin">Mensagem - <span id="counter">${160 - messageLength}</span>:</div>
				<s:textarea class="textField" id="messageBox" name="message" style="width:400px; height:100px"></s:textarea>
				<div class="margin">
					<s:submit name="save" class="bttn blue" value="Salvar"></s:submit>
					<s:submit style="margin-left:15px" name="cancel" class="bttn red" value="Cancelar"></s:submit>
				</div>
			</s:form>
		</div>
		<div><s:errors></s:errors></div>
	</div>
<script type="text/javascript" src="page/js/plugin/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="page/js/plugin/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript" src="page/js/smsEdit.js"></script>
</body>
</html>