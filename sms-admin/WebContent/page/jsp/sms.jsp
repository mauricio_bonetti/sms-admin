<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Torpedo-Avisa</title>
<link rel="stylesheet" type="text/css" href="page/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="page/css/commons.css"/>
<link rel="stylesheet" type="text/css" href="page/css/css3-gmail-style.css"/>
<link rel="stylesheet" type="text/css" href="page/css/content.css"/>
<link rel="stylesheet" type="text/css" href="page/css/header.css"/>
<link rel="stylesheet" type="text/css" href="page/css/menu.css"/>
</head>
<body>
	<!-- Header -->
	<%@ include file="tiles/header.jsp" %>
	<!-- Menu -->
	<%@ include file="tiles/menu.jsp" %>
	<!-- Content -->
	<div class="content common">
		<span>SMS</span>
		<div style="margin-top:20px;">
			<span>Agenda para envios de sms do dia <a style="font-weight:bold" href="#" id="timestamp">${actionBean.formattedDate}</a>. Clique na data para visualizar outras.</span>
		</div>
		<div style="margin-top:20px;">
			<c:if test="${not actionBean.dayPast}">
				<s:link class="bttn blue common" beanclass="com.bon.action.SMSAddActionBean" event="main">
					<s:param name="timestamp" value="${actionBean.timestamp}"/> 
					Adicionar
				</s:link>
			</c:if>
		</div>
		<div style="margin-top:10px; margin-right:15px; margin-bottom:15px;">
			<table class="table">
				<tr>
					<th>Ação</th>
					<th>Horário</th>
					<th>Cliente</th>
					<th>Mensagem</th>
				</tr>
				<c:forEach items="${actionBean.smss}" var="sms">
					<tr>
						<td class="action">
							<c:if test="${not sms.scheduledTimePast}">
								<s:form style="width:60px;display:inline-block" beanclass="com.bon.action.SMSEditActionBean" method="GET">
									<input type="hidden" name="objectId" value="${sms.objectId}"/>
									<input type="hidden" name="timestamp" value="${actionBean.timestamp}"/>
									<s:submit class="bttn" name="main">Editar</s:submit>
								</s:form>
								<s:form style="width:60px;display:inline-block" beanclass="com.bon.action.SMSDeleteActionBean" method="GET">
									<input type="hidden" name="objectId" value="${sms.objectId}"/>
									<input type="hidden" name="timestamp" value="${actionBean.timestamp}"/>
									<input type="hidden" name="scheduledTimestamp" value="${sms.scheduledTimestamp}"/>
									<s:submit class="bttn" name="delete" onclick="return confirm('Confirma a exclusão?');">Excluir</s:submit>
								</s:form>
							</c:if>
						</td>
						<td>${sms.scheduledTimestampFormatted}</td>
						<td class="customer">${sms.customerName}</td>
						<td class="message">${sms.message}</td>
					</tr>
				</c:forEach>
			</table>			
		</div>
	</div>
<script type="text/javascript" src="page/js/plugin/scw.js"></script>
<script type="text/javascript" src="page/js/plugin/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="page/js/sms.js"></script>
</body>
</html>