<%@page contentType="text/html" pageEncoding="UTF-8" %>
<div class="header">
	<div style="margin-top:5px">
		<div class="logo"></div>
		<div class="common greetings">Olá, ${sessionScope.user.name}</div>
		<div class="exit">
			<s:link class="bttn red" beanclass="com.bon.action.LoginActionBean" event="_logout">Sair</s:link>
		</div>
	</div>
</div>