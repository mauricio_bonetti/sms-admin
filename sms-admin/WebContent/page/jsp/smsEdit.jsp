<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@taglib  prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Torpedo-Avisa</title>
<link rel="stylesheet" type="text/css" href="page/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="page/css/commons.css"/>
<link rel="stylesheet" type="text/css" href="page/css/css3-gmail-style.css"/>
<link rel="stylesheet" type="text/css" href="page/css/content.css"/>
<link rel="stylesheet" type="text/css" href="page/css/header.css"/>
<link rel="stylesheet" type="text/css" href="page/css/menu.css"/>
</head>
<body>
	<!-- Header -->
	<%@ include file="tiles/header.jsp" %>
	<!-- Menu -->
	<%@ include file="tiles/menu.jsp" %>
	<!-- Content -->
	<div class="content common" style="width:420px">
		<span>SMS</span>
		<div style="margin-top:20px;">
			<span>Editar SMS para o dia ${actionBean.formattedDate}</span>
		</div>
		<div style="margin-top:10px; margin-right:15px; margin-bottom:15px;">
			<s:form beanclass="com.bon.action.SMSEditActionBean" method="POST">
				<s:hidden name="timestamp" value="${actionBean.timestamp}"></s:hidden>
				<s:hidden name="objectId" value="${actionBean.objectId}"></s:hidden>
				<div class="margin">Horário: <s:text id="scheduledTime" style="width:40px" name="scheduledTime" class="textField" value="${actionBean.sms.scheduledTimestampFormatted}"></s:text></div>			
				<div class="margin">Cliente: 
					<select name="customerObjectId" class="textField">
						<c:forEach items="${actionBean.activeCustomers}" var="customer">
							<c:set var="checked" value="${actionBean.sms.customerObjectId == customer.objectId}"/>
							<c:choose>
								<c:when test="${checked}">
									<option selected value="${customer.objectId}">${customer.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${customer.objectId}">${customer.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
				<c:set var="messageLength" value="${fn:length(actionBean.sms.message)}"></c:set>
				<c:if test="${messageLength == 0}">
					<c:set var="messageLength" value="${fn:length(actionBean.message)}"></c:set>
				</c:if>
				<div class="margin">Mensagem - <span id="counter">${160 - messageLength}</span>:</div>
				<s:textarea class="textField" id="messageBox" name="message" style="width:400px; height:100px" value="${actionBean.sms.message}"></s:textarea>
				<div class="margin">
					<s:submit name="save" class="bttn blue" value="Salvar"></s:submit>
					<s:submit style="margin-left:15px" name="cancel" class="bttn red" value="Cancelar"></s:submit>
				</div>
			</s:form>
		</div>
		<div><s:errors></s:errors></div>
	</div>
<script type="text/javascript" src="page/js/plugin/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="page/js/plugin/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript" src="page/js/smsEdit.js"></script>
</body>
</html>