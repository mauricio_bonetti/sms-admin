<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Torpedo-Avisa</title>
<link rel="stylesheet" type="text/css" href="page/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="page/css/commons.css"/>
<link rel="stylesheet" type="text/css" href="page/css/css3-gmail-style.css"/>
<link rel="stylesheet" type="text/css" href="page/css/content.css"/>
<link rel="stylesheet" type="text/css" href="page/css/header.css"/>
<link rel="stylesheet" type="text/css" href="page/css/menu.css"/>
</head>
<body>
	<!-- Header -->
	<%@ include file="tiles/header.jsp" %>
	<!-- Menu -->
	<%@ include file="tiles/menu.jsp" %>
	<!-- Content -->
	<div class="content common">
		<span>Cliente</span>
		<div style="margin-top:20px;">
			<s:link class="bttn blue common" beanclass="com.bon.action.CustomerAddActionBean" event="main">Adicionar</s:link>
		</div>
		<div style="margin-top:10px; margin-right:15px; margin-bottom:15px;">
			<table class="table">
				<tr>
					<th>Ação</th>
					<th>Nome</th>
					<th>Celular</th>
				</tr>
				<c:forEach items="${actionBean.customers}" var="customer">
					<tr>
						<td class="action">
							<s:form style="width:60px;display:inline-block" beanclass="com.bon.action.CustomerEditActionBean" method="GET">
								<input type="hidden" name="customer.objectId" value="${customer.objectId}"/>
								<s:submit class="bttn" name="main">Editar</s:submit>
							</s:form>
							<s:form style="width:60px;display:inline-block" beanclass="com.bon.action.CustomerDeleteActionBean" method="GET">
								<input type="hidden" name="customer.objectId" value="${customer.objectId}"/>
								<s:submit class="bttn" name="delete" onclick="return confirm('Excluir um cliente irá cancelar todas as mensagens agendadas para esse contato. Confirma?');">Excluir</s:submit>
							</s:form>
						</td>
						<td>${customer.name}</td>
						<td>${customer.msisdn}</td>
					</tr>
				</c:forEach>
			</table>			
		</div>
	</div>
</body>
</html>