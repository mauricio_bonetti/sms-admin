<%@taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Torpedo-Avisa</title>
<link rel="stylesheet" type="text/css" href="page/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="page/css/commons.css"/>
<link rel="stylesheet" type="text/css" href="page/css/css3-gmail-style.css"/>
<link rel="stylesheet" type="text/css" href="page/css/login.css"/>
</head>
<body>
	<div id="logo" class="center margin"></div>
	<div class="center formPanel">
		<p class="common center formText">Login</p>
		<div class="center form">
			<s:form beanclass="com.bon.action.LoginActionBean" method="POST">
				<p class="common bold margin">Email:</p>
				<s:text class="textField" id="email" name="email"></s:text>
				<p class="common bold margin">Senha:</p>
				<s:password class="textField" id="password" name="password"></s:password>
				<br/>
				<s:submit name="login" class="bttn blue margin" value="Login"></s:submit>
				<a class="common" href="#">Não consegue acessar?</a>
				<s:errors></s:errors>
			</s:form>
		</div>
	</div>
</body>
</html>