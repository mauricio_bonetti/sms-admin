function openCalendar(event) {
	scwShow($("#timestamp")[0], event);
}

function getMonthCode(month) {
	var monthCode;
	switch (month) {
		case "Jan":
			monthCode = 0;
			break;
		case "Fev":
			monthCode = 1;
			break;
		case "Mar":
			monthCode = 2;
			break;
		case "Abr":
			monthCode = 3;
			break;
		case "Mai":
			monthCode = 4;
			break;
		case "Jun":
			monthCode = 5;
			break;
		case "Jul":
			monthCode = 6;
			break;
		case "Ago":
			monthCode = 7;
			break;
		case "Set":
			monthCode = 8;
			break;
		case "Out":
			monthCode = 9;
			break;
		case "Nov":
			monthCode = 10;
			break;
		case "Dez":
			monthCode = 11;
			break;
	}
	return monthCode;
}

function goToDate(event) {
	var date = $("#timestamp").html();
	var pieces = date.split(" ");
	var ts = new Date(pieces[2], getMonthCode(pieces[1]), pieces[0]);
	window.location.href = "SMS.action?main=&timestamp=" + ts.getTime();
}

$(document).ready(function(){
	$("#timestamp").click(openCalendar);
	$("#timestamp").bind("DOMSubtreeModified", goToDate); 
});