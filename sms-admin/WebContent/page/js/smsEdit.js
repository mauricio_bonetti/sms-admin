var SMS_MAX_LENGTH = 160;

function messageValidator(event) {
	var message = $("#messageBox").val();
	if (event.keyCode == 13) {
		$("#messageBox").val(message.slice(0, -1)); // remove last char
	}
	if (message.length > SMS_MAX_LENGTH) {
		$("#messageBox").val(message.slice(0, SMS_MAX_LENGTH)); // remove last char
		alert("A mensagem não deve ultrapassar " + SMS_MAX_LENGTH + " caracteres");
		return false;
	} else {
		$("#counter").html(SMS_MAX_LENGTH - message.length);
	}
}

function filterMessage(event) {
	var varString = new String($("#messageBox").val());
	var accent = new String('àâêôûãõáéíóúçüÀÂÊÔÛÃÕÁÉÍÓÚÇÜ');
	var noAccent = new String('aaeouaoaeioucuAAEOUAOAEIOUCU');
	 
	var i = new Number();
	var j = new Number();
	var cString = new String();
	var varRes = '';
	for (i = 0; i < varString.length; i++) {
		cString = varString.substring(i, i + 1);
		for (j = 0; j < accent.length; j++) {
			if (accent.substring(j, j + 1) == cString) {
				cString = noAccent.substring(j, j + 1);
			}
		}
		varRes += cString;
	}
	$("#messageBox").val(varRes);
}

$(document).ready(function() {
	$("#scheduledTime").mask("99:99");
	$("#messageBox").keyup(messageValidator);
	$("#messageBox").keyup(filterMessage);
});